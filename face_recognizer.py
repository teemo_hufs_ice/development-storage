#!/usr/bin/python


# Import the required modules
import cv2
import os, time, re, urllib
import numpy as np
from PIL import Image
import threading,Queue

# For face detection we will use the Haar Cascade provided by OpenCV.
cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)

lock = threading.Lock()


##########################
def detection_face():

    faceCascade = cv2.CascadeClassifier('./haarcascade_frontalface_default.xml')

    video_capture = cv2.VideoCapture(0)

    while True:
        # Capture frame-by-frame
        ret, frame = video_capture.read()

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(50, 50),
            flags=cv2.cv.CV_HAAR_SCALE_IMAGE
        )

        # Draw a rectangle around the faces
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
            
            crop_img = frame[y:y+h,x:x+w]
            with lock:
                cv2.imwrite('compare.jpg', frame)
           # similarity = image_similarity_bands_via_numpy()
            #print "image_similarity : %s" %(similarity)

        # Display the resulting frame
        cv2.imshow('Video', frame)


        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    
    # When everything is done, release the capture
    video_capture.release()
    cv2.destroyAllWindows()
###########################################

# For face recognition we will the the LBPH Face Recognizer 
recognizer = cv2.createLBPHFaceRecognizer()

def get_images_and_labels(path):
    # put all the images of database in image_paths
    image_paths = [os.path.join(path, f) for f in os.listdir(path) ]
    #  contains face images
    images = []
    # contain the images name
    labels = []
    for image_path in image_paths:
        # Read image and change to gray
        if image_path != 'Thumbs.db':
            image_pil = Image.open(image_path).convert('L')
        # change image to numpy array
            image = np.array(image_pil, 'uint8')
        # get name of image
            nbr = int(os.path.split(image_path)[1].split(".")[0])
        print nbr
        # detect the face
        faces = faceCascade.detectMultiScale(image)
        
        for (x, y, w, h) in faces:
            images.append(image[y: y + h, x: x + w])
            labels.append(nbr)
            cv2.waitKey(50)
    # return the images list and labels list
    return images, labels

# Path to the Yale Dataset
path = './yalefaces'
# Call the get_images_and_labels function and get the face images and the 
# corresponding labels
images, labels = get_images_and_labels(path)
cv2.destroyAllWindows()

# Perform the tranining
recognizer.train(images, np.array(labels))

# open the camera and take the picture of faces 
thread = threading.Thread(target = detection_face,args = ())
thread.daemon = True
thread.start()



while True:
    image_path = 'compare.jpg'
    # prtect the critical region	
    with lock:
        predict_image_pil = Image.open(image_path).convert('L')
        predict_image = np.array(predict_image_pil, 'uint8')
        faces = faceCascade.detectMultiScale(predict_image)
    for (x, y, w, h) in faces:
        nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
        nbr_actual = image_path
        if conf <= 27:
            print "match with : {}  confidence :  {}".format(nbr_predicted, conf)
        else:
            print "not match people. looks like {} . confidence : {}".format(nbr_predicted,conf)
        cv2.imshow("Recognizing Face", predict_image[y: y + h, x: x + w])
        cv2.waitKey(1000)
