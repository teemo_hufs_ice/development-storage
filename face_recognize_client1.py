#-*- coding: utf-8 -*-.
import cv2
import os, time, re, urllib
import numpy as np
from PIL import Image
import threading
import webbrowser
import sys, socket

cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)
recognizer = cv2.createLBPHFaceRecognizer()




lock = threading.Lock()

def detection_face():

    faceCascade = cv2.CascadeClassifier('./haarcascade_frontalface_default.xml')

    video_capture = cv2.VideoCapture(0)

    while True:
        # Capture frame-by-frame
        ret, frame = video_capture.read()
        print type(frame)
        cv2.imshow('Video', frame)

        with lock:
            cv2.imwrite('compare.jpg', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    
    # When everything is done, release the capture
    video_capture.release()
    cv2.destroyAllWindows()

thread = threading.Thread(target = detection_face,args = ())
thread.daemon = True
thread.start()

server_addr1 = ('localhost', 50026)
server_addr2 = ('localhost', 50027)

sock1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock1.connect(server_addr1)        # connect to server process

sock2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock2.connect(server_addr2)        # connect to server process

image_path = 'compare.jpg'

def sending_img(sock2,image_path):
    while True:
        with lock:
            #frame = cv2.imread(image_path,0)
            image_pil = Image.open(image_path).convert('L')
        frame = np.array(image_pil, 'uint8')
#추출한 이미지를 String 형태로 변환(인코딩)시키는 과정
        encode_param=[int(cv2.IMWRITE_JPEG_QUALITY),90]
        result, imgencode = cv2.imencode('.jpg', frame, encode_param)
        data = np.array(imgencode)
        stringData = data.tostring()

#String 형태로 변환한 이미지를 socket을 통해서 전송
        sock2.send( str(len(stringData)).ljust(16));
        sock2.send( stringData );
        #print sock2.recv(40)

def sending(sock1):
    data = sock1.recv(20)
    
sending_img(sock2,image_path)
"""

def echo_client(server_addr):
 
    # make TCP/IP socket obj
    image_path = 'compare.jpg'
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(server_addr)        # connect to server process
    while True:
        message = raw_input("input : ")
        sock.send(message)
        if message == 'log':
            frame = cv2.imread('compare.jpg')

            #추출한 이미지를 String 형태로 변환(인코딩)시키는 과정
            encode_param=[int(cv2.IMWRITE_JPEG_QUALITY),90]
            result, imgencode = cv2.imencode('.jpg', frame, encode_param)
            data = np.array(imgencode)
            stringData = data.tostring()

            #String 형태로 변환한 이미지를 socket을 통해서 전송
            sock.send( str(len(stringData)).ljust(16));
            sock.send( stringData );
        else:         # send message to server over socket
            print message      # receive response from server: up to 1KB
                   
    sock.close()                    # close socket to send eof to server
    
echo_client(('localhost', 50022))
"""
