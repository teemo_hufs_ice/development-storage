import cv2, os
import numpy as np
from PIL import Image

cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)
recognizer = cv2.createLBPHFaceRecognizer()
path = './ff'
image_paths = [os.path.join(path, f) for f in os.listdir(path)]

for image_path in image_paths:
    if image_path == './ff\\Thumbs.db':
        continue
    image_pil = Image.open(image_path).convert('L')
    image = np.array(image_pil, 'uint8')
        # Get the label of the image
        
        # Detect the face in the image
    faces = faceCascade.detectMultiScale(image)
    for (x, y, w, h) in faces:
        x = image[y:y+h,x:x+w]
        cv2.imwrite(image_path,x)
