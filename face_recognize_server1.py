#-*- coding: utf-8 -*-.
import cv2
import os, time, re, urllib
import numpy as np
from PIL import Image
import threading
from socket import socket, AF_INET, SOCK_STREAM


cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)

# For face recognition we will the the LBPH Face Recognizer 
recognizer = cv2.createLBPHFaceRecognizer()

def get_images_and_labels(path):
    # Append all the absolute image paths in a list image_paths
    # We will not read the image with the .sad extension in the training set
    # Rather, we will use them to test our accuracy of the training
    image_paths = [os.path.join(path, f) for f in os.listdir(path)]
    # images will contains face images
    images = []
    # labels will contains the label that is assigned to the image
    labels = []
    k = 1
    for image_path in image_paths:
        if image_path == './ff\\Thumbs.db':
            continue
        # Read the image and convert to grayscale
        image_pil = Image.open(image_path).convert('L')
        # Convert the image format into numpy array
        image = np.array(image_pil, 'uint8')
        # Get the label of the image
        nbr =int( os.path.split(image_path)[1].split(".")[0])
        # Detect the face in the image

        images.append(image)
        labels.append(nbr)
        k= k+1
    # return the images list and labels list
    return images, labels

path = './ff'
# Call the get_images_and_labels function and get the face images and the 
# corresponding labels

images, labels = get_images_and_labels(path)
cv2.destroyAllWindows()

print labels

# Perform the tranining
recognizer.train(images, np.array(labels))

t = ''


html = """<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>

	<title>Family Mirror</title>
	<!-- css -->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/pignose.calendar.css" />
	<!-- javascript -->
	<script type="text/javascript" src="js/vendor/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="js/vendor/prefixfree.min.js"></script>
	<script type="text/javascript" src="js/vendor/moment.min.js"></script>
	<script type="text/javascript" src="js/pignose.calendar.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
	<script src="https://apis.google.com/js/client.js"></script>
	<script src="http://www.youtube.com/iframe_api"></script>
</head>

<body>
	<header>
		Family Mirror Header
	</header>
	<div class="contents">
		<aside id="left_content">
			<div class="white" id="date"></div>
			<div class="white" class="article">
            	<div class="disabled-dates-calendar"></div>
			</div>
			<div class="white" id="schedule"></div>
		</aside>
		<aside id="middle_content">
			<div class="white" id="name"></div>
			<div class="temp"></div>
			<div class="white" id="ment"></div>
			<div class="temp"></div>
			<div class="white" id="schedule_detail" style="display: none"></div>
			<div class="white" id="weather_detail" style="display: none"></div>
			<div class="white" id="mail_detail" style="display: none"></div>
			<div id="youtube" style="display: none"></div>
		</aside>
		<aside id="right_content">
			<div class="white" id="time"></div>
			<div id="weather_con"></div>
			<div class="white" id="weather"></div>
			<div class="temp"></div>
			<div class="white" id="mail"><a target="_blank" title="follow me on google plus" href="https://plus.google.com/PLACEHOLDER"><img alt="follow me on google plus" src="https://c866088.ssl.cf3.rackcdn.com/assets/googleplus30x30.png" border=0></a></div>
			<div class="temp"></div>
			<a target="_blank" title="follow me on youtube" href="http://www.youtube.com/PLACEHOLDER"><img alt="follow me on youtube" src="https://c866088.ssl.cf3.rackcdn.com/assets/youtube40x40.png" border=0></a>

			<div class="white">
			Youtube
			</div>
		</aside>
	</div>
	<div class="white" id="speech"></div>
	<div id="logIdHint" style="display: none">%(result)s</div>
	<footer class="white">
	 Copyright 2016. TEEMO all rights reserved.
	</footer>
</body>
</html>"""




def receive_img(conn2,recognizer):
    global t
    while True:
        length = recvall(conn2,16) #길이 16의 데이터를 먼저 수신하는 것은 여기에 이미지의 길이를 먼저 받아서 이미지를 받을 때 편리하려고 하는 것이다.
        stringData = recvall(conn2, int(length))
        data = np.fromstring(stringData, dtype='uint8')
        print "x"
    #s.close()
        decimg=cv2.imdecode(data,1)
        predict_image = np.array(decimg, 'uint8')
        faces = faceCascade.detectMultiScale(predict_image)
        print "y"
        for (x, y, w, h) in faces:
            crop_img = predict_image[y: y + h, x: x + w]
            cv2.imwrite("ssi.jpg",crop_img)
            #image_pil = cv2.imread("ss.jpg",0)
            image_pil = Image.open("ssi.jpg").convert('L')
            crop_img = np.array(image_pil, 'uint8')
            nbr_predicted, conf = recognizer.predict(crop_img)
            print nbr_predicted, conf
            #conn2.send(str((nbr_predicted, conf)).ljust(40))


def recvall(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf

my_port1 = 50026
my_port2 = 50027

sock1 = socket(AF_INET, SOCK_STREAM) # make listening socket
sock1.bind(('', my_port1))        # bind it to server port number
sock1.listen(5)
conn1, cli_addr1 = sock1.accept() 

sock2 = socket(AF_INET, SOCK_STREAM) # make listening socket
sock2.bind(('', my_port2))        # bind it to server port number
sock2.listen(5)
conn2, cli_addr = sock2.accept() 


#thread = threading.Thread(target = receive_img,args = (conn2,recognizer))
#thread.start()
receive_img(conn2,recognizer)
List1 = ['tripleh0116@gmail.com','chanwo1993@gmail.com']

def log_in(sock1,conf):
    global html
    response = html%{
        'result' : register[name]
        }
    f = open('result.html')
    f.write(response)
    sock1.send(str(conf).ljust(20))

    


"""
def echo_server(my_port):
     
    sock = socket(AF_INET, SOCK_STREAM) # make listening socket
    sock.bind(('', my_port))        # bind it to server port number
                                    # '' = all available interfaces on host
    sock.listen(5)                  # listen, allow 5 pending connects
    print 'Server started'
    while True:                     # do forever (until process killed)
        conn, cli_addr = sock.accept()  # wait for next client connect
                                    # conn: new socket, addr: client addr
        print 'Connected by', cli_addr
          # recv next message on connected socket
        
        while True:
            data = conn.recv(3)
            print data
            print "ss"
            if data == 'log':
                print 'ss'
                length = recvall(conn,16) #길이 16의 데이터를 먼저 수신하는 것은 여기에 이미지의 길이를 먼저 받아서 이미지를 받을 때 편리하려고 하는 것이다.
                stringData = recvall(conn, int(length))
                data = np.fromstring(stringData, dtype='uint8')
                #s.close()
                decimg=cv2.imdecode(data,1)
                predict_image = np.array(decimg, 'uint8')
                faces = faceCascade.detectMultiScale(predict_image)
                for (x, y, w, h) in faces:
                    crop_img = predict_image[y: y + h, x: x + w]
                    cv2.imshow('SERVER',crop_img)
            else:
                print 'Server received:', data
             #   conn.send(data)         # send a reply to the client
          
        print 'Client closed', cli_addr
        conn.close()                # close the connected socket
        

echo_server(50022)
"""
