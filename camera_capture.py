import cv2
import sys
import os, time, re, urllib
from PIL import Image

def detection_face():

    faceCascade = cv2.CascadeClassifier('./haarcascade_frontalface_default.xml')

    video_capture = cv2.VideoCapture(0)

    for i in range(1101,1201,1):
        # Capture frame-by-frame
        ret, frame = video_capture.read()

        #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        #faces = faceCascade.detectMultiScale(
        #    gray,
        #    scaleFactor=1.1,
        #    minNeighbors=5,
        #    minSize=(50, 50),
        #    flags=cv2.cv.CV_HAAR_SCALE_IMAGE
        #)

        # Draw a rectangle around the faces
        #for (x, y, w, h) in faces:
        #    cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
        #    crop_img = frame[y:y+h,x:x+w]
        cv2.imwrite('3.'+str(i)+'.jpg', frame)
        #    time.sleep(1)
           # similarity = image_similarity_bands_via_numpy()
            #print "image_similarity : %s" %(similarity)

        # Display the resulting frame
        cv2.imshow('Video', frame)


        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    
    # When everything is done, release the capture
    video_capture.release()
    cv2.destroyAllWindows()

    
def image_similarity_bands_via_numpy():
    import math
    import operator
    import numpy
    image1 = Image.open('./origin.jpg')
    image2 = Image.open('compare.jpg')
 
    # create thumbnails - resize em
    image1 = get_thumbnail(image1)
    image2 = get_thumbnail(image2)
    
    # this eliminated unqual images - though not so smarts....
    if image1.size != image2.size or image1.getbands() != image2.getbands():
        return -1
    s = 0
    for band_index, band in enumerate(image1.getbands()):
        m1 = numpy.array([p[band_index] for p in image1.getdata()]).reshape(*image1.size)
        m2 = numpy.array([p[band_index] for p in image2.getdata()]).reshape(*image2.size)
        s += numpy.sum(numpy.abs(m1-m2))
    return s
 
def get_thumbnail(image, size=(128,128), stretch_to_fit=False, greyscale=False):
    " get a smaller version of the image - makes comparison much faster/easier"
    if not stretch_to_fit:
        image.thumbnail(size, Image.ANTIALIAS)
    else:
        image = image.resize(size); # for faster computation
    if greyscale:
        image = image.convert("L")  # Convert it to grayscale.
    return image
    
if __name__ == "__main__":
    detection_face()
